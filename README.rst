INSTALLATION
============

You need Python.
You need to run
``pip install -r requirements.txt``

You either need to install an app that has the following permissions on slack:

``users.profile:read``
``users.profile.write``

Then get the OAuth token (form ``xoxp-xxxx``) and put it in the proper environment variable in ``run.sh``


Or find the already existing app, visit ``https://api.slack.com/apps/<appid>/oauth``
 and get the oauth token there.


Running
-------

If you run the script once, it will try to authenticate your lastfm account with a web browser.
Then fill in the proper Env keys in run.sh

after that with the lastfm key, secret, username, and at least one (space separated) slack oauth token, you can run
./run.sh and it will update your status with the currently playing song if one is being scrobbled.
